const data = [];
const dataStudentToId = [];

function getData() {
    const table = document.getElementById('tableStudent');
    const arrayLigne = table.rows;
    for (let i = 1; i < arrayLigne.length; i++)
        {
            const nom = replaceAll(arrayLigne[i].innerText, '\t', ' ');
            data[nom] = null;
            dataStudentToId[nom] = arrayLigne[i].attributes.getNamedItem("idstudent").value;
        }
}

document.addEventListener('DOMContentLoaded', function() {
    const elems = document.querySelectorAll('.autocomplete1');
    getData();
    M.Autocomplete.init(elems, {
        data : data,
        onAutocomplete: function(val) {
            const value = $('input.autocomplete1').val();
            document.location.href = '/suivi/profile/' + dataStudentToId[value];
    }
    });
  });

function replaceAll(machaine, chaineARemaplacer, chaineDeRemplacement) {
   return machaine.replace(new RegExp(chaineARemaplacer, 'g'),chaineDeRemplacement);
 }
