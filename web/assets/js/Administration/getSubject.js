let listRadio = [];

window.addEventListener("load", function () {
    const radioList = document.querySelectorAll("input[type=radio]");
    for (let i = 0; i < radioList.length; i++) {
        radioList[i].addEventListener("click", updateCheckValues);
        listRadio.push(radioList[i]);
    }
});

function updateCheckValues(event) {
    event.stopPropagation();
    if (event.target.checked)
        listRadio.forEach(elem => {
            if (getId(elem) === getId(event.target) && elem.name !== event.target.name) {
                elem.checked = false;
            }
        });
}

function getId(radioElem) {
    return radioElem.name.split('[')[2].charAt(0);
}
