const data = [];
const dataToTeacherId = [];

function getData()
{
    const table = document.getElementById('tableTeacher');
    const arrayLigne = table.rows;
    for (let i = 1; i < arrayLigne.length; i++)
        {
            const nom = replaceAll(arrayLigne[i].innerText,'\t',' ');
            data[nom] = null;
            dataToTeacherId[nom] = arrayLigne[i].attributes.getNamedItem("idteacher").value;
        }
}

document.addEventListener('DOMContentLoaded', function() {
    const elems = document.querySelectorAll('.autocomplete2');
    getData();
    M.Autocomplete.init(elems, {
        data : data,
        onAutocomplete: function(val) {
        const value = $('input.autocomplete2').val();
        document.location.href='/administration/professeur/modifier/' + dataToTeacherId[val];
    }
    });
  });

function replaceAll(machaine, chaineARemaplacer, chaineDeRemplacement) {
   return machaine.replace(new RegExp(chaineARemaplacer, 'g'),chaineDeRemplacement);
 }
