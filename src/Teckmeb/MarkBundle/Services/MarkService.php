<?php


namespace Teckmeb\MarkBundle\Services;


use Doctrine\ORM\EntityManager;
use Teckmeb\MarkBundle\Model\NoteView;
use Teckmeb\UserBundle\Entity\User;

class MarkService
{
    protected $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getNoteViewFromUser(User $user = null)
    {
        $student = $this->doctrine->getRepository('TeckmebCoreBundle:Student')->findOneByUser($user);
        $noteView = new NoteView($student->getGroupes());
        $repository = $this->doctrine->getRepository('TeckmebMarkBundle:Mark');
        foreach ($noteView->getGroupeList() as $groupeObj) {
            $sommeMoyenne = 0;
            $sommeDiviseur = 0;
            foreach ($groupeObj->getSubjectList() as $subjectObj) {
                if (($moyenne = $subjectObj->initMarkList($repository->myFindAllMark($student, $groupeObj->getGroupe(), $subjectObj->getSubject()))) != null) {
                    $sommeMoyenne += $moyenne * $subjectObj->getSubject()->getSubjectCoefficient();
                    $sommeDiviseur += 20 * $subjectObj->getSubject()->getSubjectCoefficient();
                }
            }
            $groupeObj->setMoyenne(($sommeDiviseur == 0) ? null : $sommeMoyenne / $sommeDiviseur * 20);
        }
        return $noteView;
    }
}
