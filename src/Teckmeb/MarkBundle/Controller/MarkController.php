<?php

namespace Teckmeb\MarkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MarkController extends Controller
{
    public function verification($control)
    {
        if ($control->getEducation() != null) {
            return ($control->getEducation()->getTeacher()->getUser()->getId() == $this->getUser()->getId());
        } else {
            $teacher = $this->getDoctrine()->getManager()->getRepository('TeckmebCoreBundle:Teacher')->findOneByUser($this->getUser());
            $repository = $this->getDoctrine()->getManager()->getRepository('TeckmebCoreBundle:Education');
            return (count($repository->myVerification($control, $teacher)) > 0);
        }
    }

    public function viewAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux étudiants.');
        }

        return $this->render('TeckmebMarkBundle::mark_view.html.twig', array(
            'noteView' => $this->get("teckmeb_mark.markService")->getNoteViewFromUser($this->getUser()),
            'Mark' => true,
        ));
    }
}
