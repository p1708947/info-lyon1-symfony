<?php


namespace Teckmeb\MarkBundle\Model;


class NoteView
{
    private $groupeList;

    public function __construct($groupeList)
    {
        $this->groupeList = array();
        foreach ($groupeList as $group) {
            $this->groupeList[] = new GroupView($group);
        }
    }

    /**
     * @return mixed
     */
    public function getGroupeList()
    {
        return $this->groupeList;
    }

    /**
     * @param mixed $groupeList
     */
    public function setGroupeList($groupeList): void
    {
        $this->groupeList = $groupeList;
    }


}
