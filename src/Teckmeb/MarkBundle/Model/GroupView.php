<?php


namespace Teckmeb\MarkBundle\Model;


use Teckmeb\CoreBundle\Entity\Groupe;

class GroupView
{
    private $groupe;
    private $subjectList;
    private $moyenne;

    public function __construct(Groupe $groupe)
    {
        $this->subjectList = array();
        $this->groupe = $groupe;
        foreach ($this->groupe->getSemestre()->getCourse()->getTeachingUnits() as $tu) {
            foreach ($tu->getModules() as $module) {
                foreach ($module->getSubjects() as $subject) {
                    $this->subjectList[] = new SubjectView($subject);
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getSubjectList()
    {
        return $this->subjectList;
    }

    /**
     * @param mixed $subjectList
     */
    public function setSubjectList($subjectList): void
    {
        $this->subjectList = $subjectList;
    }

    /**
     * @return Groupe
     */
    public function getGroupe(): Groupe
    {
        return $this->groupe;
    }

    /**
     * @param Groupe $groupe
     */
    public function setGroupe(Groupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return mixed
     */
    public function getMoyenne()
    {
        return $this->moyenne;
    }

    /**
     * @param mixed $moyenne
     */
    public function setMoyenne($moyenne): void
    {
        $this->moyenne = $moyenne;
    }

    public function __toString()
    {
        return $this->groupe->getSemestre()->getSchoolYear() . " " . $this->groupe->getSemestre()->getCourse()->getCourseType()->__toString() . " - " . $this->groupe->__toString();
    }
}
