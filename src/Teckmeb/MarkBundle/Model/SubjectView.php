<?php


namespace Teckmeb\MarkBundle\Model;


use Teckmeb\CoreBundle\Entity\Subject;

class SubjectView
{
    private $subject;
    private $markList;
    private $moyenne = null;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->markList = array();
    }

    public function initMarkList($listMark)
    {
        if ($listMark != null) {
            $sommeMark = 0;
            $sommeDiviseur = 0;
            foreach ($listMark as $mark) {
                $this->markList[] = $mark;
                $sommeMark += $mark->getValue() * $mark->getControl()->getCoefficient();
                $sommeDiviseur += $mark->getControl()->getDivisor() * $mark->getControl()->getCoefficient();
            }
            $this->moyenne = ($sommeMark / $sommeDiviseur) * 20;
        }
        return $this->moyenne;
    }

    /**
     * @return Subject
     */
    public function getSubject(): Subject
    {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject(Subject $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getMarkList(): array
    {
        return $this->markList;
    }

    /**
     * @param array $markList
     */
    public function setMarkList(array $markList): void
    {
        $this->markList = $markList;
    }

    /**
     * @return null
     */
    public function getMoyenne()
    {
        return $this->moyenne;
    }

    /**
     * @param null $moyenne
     */
    public function setMoyenne($moyenne): void
    {
        $this->moyenne = $moyenne;
    }


}
