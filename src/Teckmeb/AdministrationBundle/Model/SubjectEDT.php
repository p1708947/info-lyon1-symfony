<?php


namespace Teckmeb\AdministrationBundle\Model;


class SubjectEDT
{
    private $listUserSubject;
    private $subjectCode;
    private $userSubject;

    public function __construct($subjectCode, UserSubjectEDT $userSubjectEDT)
    {
        $this->subjectCode = $subjectCode;
        $this->listUserSubject = array();
        $this->listUserSubject[] = $userSubjectEDT;
    }

    public function add(UserSubjectEDT $userSubjectEDT) {
        foreach($this->listUserSubject as $userSubject) {
            if($userSubject->isEqualTo($userSubjectEDT)) {
                $userSubject->addCount();
                return;
            }
        }
        $this->listUserSubject[] = $userSubjectEDT;
    }

    public function clear() {
        foreach($this->listUserSubject as $userSubject) {
            if($this->userSubject == null) {
                $this->userSubject = $userSubject;
            } else if ($this->userSubject->getCount() < $userSubject->getCount()){
                $this->userSubject = $userSubject;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getUserSubject()
    {
        return $this->userSubject;
    }

    /**
     * @param mixed $userSubject
     */
    public function setUserSubject($userSubject): void
    {
        $this->userSubject = $userSubject;
    }

    /**
     * @return mixed
     */
    public function getSubjectCode()
    {
        return $this->subjectCode;
    }

    /**
     * @param mixed $subjectCode
     */
    public function setSubjectCode($subjectCode): void
    {
        $this->subjectCode = $subjectCode;
    }


}
