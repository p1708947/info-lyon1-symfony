<?php


namespace Teckmeb\AdministrationBundle\Model;


use Teckmeb\UserBundle\Entity\User;

class UserSubjectEDT
{
    private $count;
    private $listUserEDT;

    public function __construct()
    {
        $this->count = 1;
        $this->listUserEDT = array();
    }

    /**
     * @return array
     */
    public function getListUserEDT(): array
    {
        return $this->listUserEDT;
    }

    /**
     * @param array $listUserEDT
     */
    public function setListUserEDT(array $listUserEDT): void
    {
        $this->listUserEDT = $listUserEDT;
    }

    public function add(UserEDT $userEDT) {
        $this->listUserEDT[] = $userEDT;
    }

    public function getLength() {
        return count($this->listUserEDT);
    }

    public function addCount() {
        $this->count++;
    }

    public function getCount() {
        return $this->count;
    }

    public function has(UserEDT $userEDT) {
        foreach($this->listUserEDT as $user) {
            if($user->getNom() === $userEDT->getNom() && $user->getPrenom() == $userEDT->getPrenom()) {
                return true;
            }
        }
        return false;
    }

    public function isEqualTo(UserSubjectEDT $userSubjectEDT): bool {
        if($this->getLength() != $userSubjectEDT->getLength()) {
            return false;
        }
        foreach($this->listUserEDT as $userEDT) {
            if(!$userSubjectEDT->has($userEDT)) {
                return false;
            }
        }
        return true;
    }

}
