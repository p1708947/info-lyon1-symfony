<?php


namespace Teckmeb\AdministrationBundle\Model;


class ModuleEDT
{
    private $listSubject;

    public function __construct()
    {
        $this->listSubject = array();
    }

    /**
     * @return array
     */
    public function getListSubject(): array
    {
        return $this->listSubject;
    }

    /**
     * @param array $listSubject
     */
    public function setListSubject(array $listSubject): void
    {
        $this->listSubject = $listSubject;
    }

    public function add($subjectCode , UserSubjectEDT $userSubjectEDT) {
        if(isset($this->listSubject[$subjectCode])) {
            $this->listSubject[$subjectCode]->add($userSubjectEDT);
        } else {
            $this->listSubject[$subjectCode] = new SubjectEDT($subjectCode , $userSubjectEDT);
        }
    }

    public function clear() {
        foreach($this->listSubject as $subject) {
            $subject->clear();
        }
    }

}
