<?php


namespace Teckmeb\AdministrationBundle\Model;


class SubjectListEDT
{
    private $alone;
    private $aloneInBD;
    private $many;

    public function __construct(ModuleEDT $moduleEDT = null)
    {
        if($moduleEDT === null)
            return;
        $this->alone = array();
        $this->many = array();
        $this->aloneInBD = array();
        foreach($moduleEDT->getListSubject() as $subject) {
            if($subject->getUserSubject()->getLength() === 1) {
                if($subject->getUserSubject()->getListUserEDT()[0]->isInBD())
                    $this->aloneInBD[] = $subject;
                else
                    $this->alone[] = $subject;
            } else {
                $this->many[] = $subject;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getAlone()
    {
        return $this->alone;
    }

    /**
     * @param mixed $alone
     */
    public function setAlone($alone): void
    {
        $this->alone = $alone;
    }

    /**
     * @return mixed
     */
    public function getMany()
    {
        return $this->many;
    }

    /**
     * @param mixed $many
     */
    public function setMany($many): void
    {
        $this->many = $many;
    }

    /**
     * @return array
     */
    public function getAloneInBD(): array
    {
        return $this->aloneInBD;
    }

    /**
     * @param array $aloneInBD
     */
    public function setAloneInBD(array $aloneInBD): void
    {
        $this->aloneInBD = $aloneInBD;
    }


}
