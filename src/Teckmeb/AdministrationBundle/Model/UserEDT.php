<?php


namespace Teckmeb\AdministrationBundle\Model;


class UserEDT
{
    private $nom;
    private $id;
    private $prenom;
    private $isInBD;
    private $mail;
    private $isGood;

    public function __construct($nom, $prenom, $isInBD = false, $id = null)
    {
        $this->prenom = $prenom;
        $this->nom = $nom;
        $this->isInBD = $isInBD;
        $this->isGood = false;
        $this->id = $id;
        if (!$isInBD)
            $this->mail = $this->getMailUser();
    }

    private function getMailUser()
    {
        return $this->nom . '.' . $this->prenom . '@etu.univ-lyon1.fr';
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return bool
     */
    public function isInBD(): bool
    {
        return $this->isInBD;
    }

    /**
     * @param bool $isInBD
     */
    public function setIsInBD(bool $isInBD): void
    {
        $this->isInBD = $isInBD;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getIsGood()
    {
        return $this->isGood;
    }

    /**
     * @param mixed $isGood
     */
    public function setIsGood($isGood): void
    {
        $this->isGood = $isGood;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

}
