<?php

namespace Teckmeb\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Teckmeb\CoreBundle\Entity\Subject;
use Teckmeb\CoreBundle\Form\SubjectType;

class SubjectController extends Controller
{
    public function homeAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $subject = new Subject();
        $formAjoutSubject = $this->get('form.factory')->create(SubjectType::class, $subject);
        if ($request->isMethod('POST') && $formAjoutSubject->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subject);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Sujet bien ajouté');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Subject');
        $listSubject = $repository->findAll();

        return $this->render("@TeckmebAdministration/Sujet/home.html.twig", array(
            "formAjoutSubject" => $formAjoutSubject->createView(),
            "listSubject" => $listSubject
        ));
    }

    public function modifySubjectAction($id, Request $request)
    {
        $session = $request->getSession();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $repository = $this->getDoctrine()->getManager()->getRepository('TeckmebCoreBundle:Subject');
        $subject = $repository->find($id);
        if (null === $subject) {
            throw new NotFoundHttpException("Ce sujet d'id " . $id . " n'existe pas.");
        }
        $form = $this->createForm(SubjectType::class, $subject);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subject);
            $em->flush();
            $session->getFlashBag()->add('info', 'Le sujet a bien été modifié');
            return $this->redirectToRoute('teckmeb_administration_sujet_homepage');
        }
        return $this->render('TeckmebAdministrationBundle:Sujet:modify.html.twig', array(
            'form' => $form->createView(),
            'Administration' => true,
        ));
    }
}
