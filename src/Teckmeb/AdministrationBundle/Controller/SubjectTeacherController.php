<?php

namespace Teckmeb\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Teckmeb\AdministrationBundle\Form\SubjectListEDTType;
use Teckmeb\AdministrationBundle\Model\ModuleEDT;
use Teckmeb\AdministrationBundle\Model\SubjectEDT;
use Teckmeb\AdministrationBundle\Model\SubjectListEDT;
use Teckmeb\AdministrationBundle\Model\UserEDT;
use Teckmeb\AdministrationBundle\Model\UserSubjectEDT;
use Teckmeb\CoreBundle\Entity\Education;
use Teckmeb\CoreBundle\Entity\Groupe;
use Teckmeb\CoreBundle\Entity\Promo;
use Teckmeb\CoreBundle\Entity\Subject;
use Teckmeb\CoreBundle\Entity\Teacher;
use Teckmeb\UserBundle\Entity\User;

class SubjectTeacherController extends Controller
{

    private function addToBD(SubjectListEDT $subjectListEDT, Groupe $groupe)
    {
        $this->addSubjectList($subjectListEDT->getAlone(), $groupe);
        $this->addSubjectList($subjectListEDT->getAloneInBD(), $groupe);
        $this->addSubjectList($subjectListEDT->getMany(), $groupe);
    }

    private function addSubjectList($subjectList, Groupe $groupe)
    {
        foreach ($subjectList as $subjectEDT) {
            $subject = $this->getDoctrine()->getRepository(Subject::class)->findOneBySubjectCode($subjectEDT->getSubjectCode());
            $teacher = $this->getTeacher($subjectEDT);
            if ($teacher === null)
                break;

            $this->createEducation($groupe, $subject, $teacher);
        }
    }

    private function createEducation(Groupe $groupe, Subject $subject, Teacher $teacher)
    {
        $teacher->addSubject($subject);
        $education = $this->getEducation($groupe, $subject);
        $education->setTeacher($teacher);
        $this->getDoctrine()->getManager()->persist($education);
    }

    private function getEducation(Groupe $groupe, Subject $subject)
    {
        $education = $this->getDoctrine()->getRepository(Education::class)->myFindEducation($subject, $groupe);
        if (count($education) > 0) {
            return $education[0];
        } else {
            $education = new Education();
            $education->setGroupe($groupe);
            $education->setSubject($subject);
            $promo = $this->getDoctrine()->getRepository(Promo::class)->myFindPromo($subject, $groupe->getSemestre());
            if ($promo === null) {
                $promo = new Promo();
                $promo->setSubject($subject);
                $promo->setSemestre($groupe->getSemestre());
                $this->getDoctrine()->getManager()->persist($promo);
            }
            return $education;
        }
    }

    private function getTeacher(SubjectEDT $subjectEDT)
    {
        if (count($subjectEDT->getUserSubject()->getListUserEDT()) === 1) {
            $teacherEDT = $subjectEDT->getUserSubject()->getListUserEDT()[0];
        } else {
            foreach ($subjectEDT->getUserSubject()->getListUserEDT() as $userSubject) {
                if ($userSubject->getIsGood()) {
                    $teacherEDT = $userSubject;
                }
            }
        }

        if (!isset($teacherEDT)) {
            return null;
        }

        if ($teacherEDT->isInBD()) {
            $user = $this->getDoctrine()->getRepository(User::class)->find($teacherEDT->getId());
            return $this->getDoctrine()->getRepository(Teacher::class)->findOneByUser($user);
        } else {
            return $this->createTeacherFromUserEDT($teacherEDT);
        }
    }

    private function createTeacherFromUserEDT(UserEDT $userEDT)
    {
        $user = $this->get('fos_user.user_manager')->createUser();
        $user->setEnabled(true);
        $user->setUsername(strtolower($userEDT->getPrenom() . "." . $userEDT->getNom()));
        $user->setName($userEDT->getPrenom());
        $user->setSurname($userEDT->getNom());
        $user->setPlainPassword("123");
        $user->setEmail($userEDT->getMail());
        $teacher = new Teacher();
        $teacher->setUser($user);
        $this->get('fos_user.user_manager')->updateUser($user);
        $this->get("teckmeb_core.mailService")->addUserMail($user, true);
        $this->get('fos_user.user_manager')->updateUser($user);
        $this->getDoctrine()->getManager()->persist($teacher);
        return $teacher;
    }

    public function SubjectsAction($id, Request $request)
    {
        // Vérification que c'est bien le secrétériat qui accède à la page
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        // On récupère la session pour pouvoir retourner un message d'erreur si besoin
        $session = $request->getSession();

        // On récupère le groupe concerné
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Groupe');
        $groupe = $repository->find($id);
        // On vérifie qu'il existe
        if ($groupe == null) {
            $session->getFlashBag()->add('info', 'Ce groupe n\'existe pas');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }

        if (($subjectListEDT = $session->get("subjectListEDT")) != null) {
            $form = $this->get('form.factory')->create(SubjectListEDTType::class, $subjectListEDT);
            if ($form->handleRequest($request)->isSubmitted()) {
                $this->addToBD($subjectListEDT, $groupe);
                $this->getDoctrine()->getManager()->flush();
                $session->set("subjectListEDT", null);
                $session->getFlashBag()->add('info', 'Les professeurs ont bien été ajoutés');
                return $this->redirectToRoute("teckmeb_administration_modify_groupe", array("id" => $id));
            }
        }

        // Récupération de la ressource via la variable POST
        list($ressource, $projectId) = $this->getRessourceAndProjectId($_POST['ressource'], $session);
        // En cas d'erreur on redirige vers la page pour récupérer l'url
        if ($ressource == null || $projectId == null) {
            return $this->redirectToRoute('teckmeb_administration_ressource', array('id' => $id));
        }
        // On récupère le contenu de l'emploi du temps
        $calendar = $this->getCalendar($ressource, $projectId, $session);
        // En cas d'erreur on redirige vers la page d'ajout de l'url
        if ($calendar == null) {
            return $this->redirectToRoute('teckmeb_administration_ressource', array('id' => $id));
        }
        // On récupère les professeurs du fichier
        $tabResultat = $this->getTeachers($calendar, $session);
        // S'il n'y a pas de cours dans l'emploi du temps on redirige vers l'ajout de l'url
        if ($tabResultat == null) {
            return $this->redirectToRoute('teckmeb_administration_ressource', array('id' => $id));
        }

        $subjectListEDT = new SubjectListEDT($tabResultat);
        $session->set("subjectListEDT", $subjectListEDT);

        $form = $this->get('form.factory')->create(SubjectListEDTType::class, $subjectListEDT);

        return $this->render("@TeckmebAdministration/Groupe/Teacher/resume.html.twig", array(
            "subjectListEDT" => $subjectListEDT,
            "form" => $form->createView()));
    }

    private function getRessourceAndProjectId($url, $session)
    {
        // On initialise le tableau qui va contenir les cours
        $data = array();
        // On récupère les attributs de l'url
        $arr = parse_url(htmlspecialchars($url));
        // On récupère les attributs GET de l'url
        $parameters = (isset($arr['query'])) ? $arr['query'] : null;
        // On vérifie qu'il y a bien des attributs GET dans la requête et principalement un attribut ressource
        if ($parameters == null) {
            $session->getFlashBag()->add('info', 'Erreur dans l\'url rentré');
            return array(null, null);
        }
        // On récupère chaque paramêtre de l'url dans un tableau
        parse_str($parameters, $data);
        if (isset($data['resources']) && isset($data['amp;projectId'])) {
            return array($data['resources'], $data['amp;projectId']);
        }
        $session->getFlashBag()->add('info', 'Erreur dans l\'url rentré');
        return array(null, null);
    }

    private function getCalendar($ressource, $projectId, $session)
    {
        $date = new \DateTime();
        $date1 = new \DateTime($date->format(\DateTimeInterface::ATOM) . " + 365 days");
        $date2 = new \DateTime($date->format(\DateTimeInterface::ATOM) . " - 365 days");
        // Création de l'url
        $url = "http://adelb.univ-lyon1.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=" . $ressource . "&projectId=" . $projectId . "&calType=ical&firstDate=" . $date2->format("Y-m-d") . "&lastDate=" . $date1->format("Y-m-d");
        // On récupère le contenu du fichier grâce à l'url
        $calendar = file_get_contents($url);
        // Vérification qu'il y a bien du contenu
        if (!$calendar) {
            $session->getFlashBag()->add('info', 'Erreur dans le numéro de ressource');
            return null;
        }
        return $calendar;
    }

    private function getTeachers($calendar, $session)
    {
        $moduleEDT = new ModuleEDT();
        $regExpCours = '/SUMMARY:(.*)/';
        $regExpDesc = '/DESCRIPTION:(.*)/';
        $coursTab = array();
        $descTab = array();
        $n = preg_match_all($regExpCours, $calendar, $coursTab, PREG_PATTERN_ORDER);
        preg_match_all($regExpDesc, $calendar, $descTab, PREG_PATTERN_ORDER);
        if ($n == 0) {
            $session->getFlashBag()->add('info', 'Erreur dans la valeur de la ressource');
            return null;
        }
        for ($j = 0; $j < $n; $j++) {
            $cours = substr($coursTab[0][$j], 8);
            $module = explode(' ', $cours)[0];
            if (preg_match('#^M[0-9]+#', $module) > 0) {
                $desc = substr($descTab[0][$j], 12);
                $desc = substr($desc, 0, mb_strripos($desc, '('));
                $desc = explode('\n', $desc);
                if (count($desc) > 2) {
                    for ($i = 2; preg_match('#^G[1-9]S[1-4]|S[1-4]$#', $desc[$i]) > 0; $i++) ;
                    $repository = $this
                        ->getDoctrine()
                        ->getManager()
                        ->getRepository('TeckmebCoreBundle:Subject');
                    $subject = $repository->findOneBySubjectCode($module);
                    if ($subject != null) {
                        $repository = $this
                            ->getDoctrine()
                            ->getManager()
                            ->getRepository('TeckmebUserBundle:User');
                        $userSubjectEDT = new UserSubjectEDT();
                        for (; $i < count($desc) - 1; $i++) {
                            if (count(explode(' ', $desc[$i])) > 1) {
                                $user = $repository->myfindUser(strtolower(explode(' ', $desc[$i])[1]), strtolower(explode(' ', $desc[$i])[0]));
                                $userSubjectEDT->add(new UserEDT(explode(' ', $desc[$i])[0], explode(' ', $desc[$i])[1], ($user != null), ($user != null) ? $user[0]->getId() : null));
                            }
                        }
                        if ($userSubjectEDT->getLength() > 0) {
                            $moduleEDT->add($subject->getSubjectCode(), $userSubjectEDT);
                        }
                    }
                }
            }
        }
        $moduleEDT->clear();
        return $moduleEDT;
    }

    public function ressourceAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Groupe');
        $groupe = $repository->find($id);
        if (null === $groupe) {
            throw new NotFoundHttpException("Ce groupe d'id " . $id . " n'existe pas.");
        }
        return $this->render('TeckmebAdministrationBundle::preSubject.html.twig', array(
            'groupe' => $groupe,
        ));
    }
}
