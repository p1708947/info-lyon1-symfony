<?php

namespace Teckmeb\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Teckmeb\CoreBundle\Entity\Student;
use Teckmeb\CoreBundle\Entity\Teacher;
use Teckmeb\CoreBundle\Form\TeacherType;
use Teckmeb\UserBundle\Form\UserEditType;

class SuiviAdministrationController extends Controller
{
    public function suiviTeacherAction($id, Request $request)
    {
        $session = $request->getSession();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Teacher');
        $teacher = $repository->find($id);
        if (null === $teacher) {
            throw new NotFoundHttpException("Ce professeur d'id " . $id . " n'existe pas.");
        }
        $formAddSubject = $this->createForm(TeacherType::class, $teacher);
        if ($request->isMethod('POST') && $formAddSubject->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($teacher);
            $em->flush();
            $session->getFlashBag()->add('info', 'Les ajouts de sujet au professeur ont bien été pris en compte');
            return $this->redirectToRoute('teckmeb_administration_suivi_teacher', array("id" => $id));
        }
        return $this->render('TeckmebAdministrationBundle:Professeur:suiviTeacher.html.twig', array(
            'formAddSubject' => $formAddSubject->createView(),
            'Administration' => true,
            'Teacher' => $teacher,
        ));
    }

    public function modifyTeacherAction($id, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Teacher');
        $teacher = $repository->find($id);
        if (null === $teacher) {
            throw new NotFoundHttpException("Cet étudiant d'id " . $id . " n'existe pas.");
        }
        $formDefineUser = $this->createForm(UserEditType::class, $teacher->getUser());
        if ($request->isMethod('POST') && $formDefineUser->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($teacher->getUser());
            $em->flush();
            $session->getFlashBag()->add('info', 'Les modifications du professeur ' . $teacher->getUser()->getSurname() . " " . $teacher->getUser()->getName() . ' ont bien été prises en compte');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }
        return $this->render('TeckmebAdministrationBundle:Professeur:updateTeacher.html.twig', array(
            'Administration' => true,
            'formDefineUser' => $formDefineUser->createView(),
            'teacher' => $teacher,
        ));
    }

    public function suiviStudentAction($id, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Student');
        $student = $repository->find($id);
        if (null === $student) {
            throw new NotFoundHttpException("Cet étudiant d'id " . $id . " n'existe pas.");
        }
        $formDefineUser = $this->createForm(UserEditType::class, $student->getUser());
        if ($request->isMethod('POST') && $formDefineUser->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($student->getUser());
            $em->flush();
            $session->getFlashBag()->add('info', 'Les modifications de l\'étudiant ' . $student->getUser()->getSurname() . " " . $student->getUser()->getName() . ' ont bien été prises en compte');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }
        return $this->render('TeckmebAdministrationBundle:Etudiant:suiviStudent.html.twig', array(
            'Administration' => true,
            'formDefineUser' => $formDefineUser->createView(),
            'student' => $student,
        ));
    }

    public function homeStudentAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Student');
        $listStudent = $repository->findAll();
        return $this->render('@TeckmebAdministration/Etudiant/home.html.twig', array(
            "listStudent" => $listStudent
        ));
    }

    public function homeProfesseurAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Teacher');
        $listTeacher = $repository->findAll();
        return $this->render("@TeckmebAdministration/Professeur/home.html.twig", array(
            "listTeacher" => $listTeacher
        ));
    }

    public function passwordChangeAction($id, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebUserBundle:User');
        $user = $repository->find($id);
        if (null === $user) {
            throw new NotFoundHttpException("Cet utilisateur d'id " . $id . " n'existe pas.");
        }
        $password = $this->get("teckmeb_core.passwordService")->getRandomPassword();
        $user->setPlainPassword($password);
        $this->get("fos_user.user_manager")->updateUser($user);

        $this->get("teckmeb_core.mailService")->updatePasswordMail($user, $password);

        $request->getSession()->getFlashBag()->add('info', "Le mot de passe à bien été réinitialisé");
        return $this->redirectToRoute("teckmeb_administration_homepage");
    }

    public function addStudentAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $user = $this->get('fos_user.user_manager')->createUser();
        $user->setEnabled(true);
        $mailService = $this->get("teckmeb_core.mailService");
        $formCreateUser = $this->get('form.factory')->create(UserEditType::class, $user);
        if ($request->isMethod('POST') && $formCreateUser->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setPlainPassword($this->get("teckmeb_core.passwordService")->generatePassword());
            $student = new Student();
            $student->setUser($user);
            $this->get('fos_user.user_manager')->updateUser($user);
            $mailService->addUserMail($user, false);
            $em->persist($user);
            $em->persist($student);
            $this->get('fos_user.user_manager')->updateUser($user, true);
            $request->getSession()->getFlashBag()->add('info', 'Etudiant bien ajouté');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }

        return $this->render("@TeckmebAdministration/Etudiant/create.html.twig", array(
            'formCreateUser' => $formCreateUser->createView()
        ));
    }

    public function addTeacherAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SECRETARIAT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat.');
        }
        $user2 = $this->get('fos_user.user_manager')->createUser();
        $user2->setEnabled(true);
        $mailService = $this->get("teckmeb_core.mailService");
        $formCreateTeacher = $this->get('form.factory')->create(UserEditType::class, $user2);
        if ($request->isMethod('POST') && $formCreateTeacher->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $this->get("teckmeb_core.passwordService")->getRandomPassword(true);
            $user2->setPlainPassword($password);
            $teacher = new Teacher();
            $teacher->setUser($user2);
            $this->get('fos_user.user_manager')->updateUser($user2);
            $mailService->addUserMail($user2, true, $password);
            $em->persist($teacher);
            $this->get('fos_user.user_manager')->updateUser($user2);
            $request->getSession()->getFlashBag()->add('info', 'Professeur bien ajouté');
            return $this->redirectToRoute('teckmeb_administration_homepage');
        }
        return $this->render("@TeckmebAdministration/Professeur/create.html.twig", array(
            "formCreateTeacher" => $formCreateTeacher->createView()
        ));
    }
}
