<?php

namespace Teckmeb\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class UserEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                "label" => "Prénom"
            ))
            ->add('surname', TextType::class, array(
                "label" => "Nom"
            ))
            ->remove('plainPassword');
    }

    public function getParent()
    {
        return UserType::class;
    }
}
