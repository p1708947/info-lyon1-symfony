<?php


namespace Teckmeb\SuiviBundle\Model;


use Teckmeb\AdministrationBundle\Entity\TeachingUnit;

class UEDTO
{
    private $ue;
    private $subjectList;
    private $moyenne;

    public function __construct(TeachingUnit $ue)
    {
        $this->ue = $ue;
        $this->moyenne = new Moyenne();
        $this->subjectList = array();
        foreach ($ue->getModules() as $module) {
            foreach ($module->getSubjects() as $subject) {
                $this->subjectList[] = new SubjectDTO($subject);
            }
        }
    }

    /**
     * @return TeachingUnit
     */
    public function getUe(): TeachingUnit
    {
        return $this->ue;
    }

    /**
     * @param TeachingUnit $ue
     */
    public function setUe(TeachingUnit $ue): void
    {
        $this->ue = $ue;
    }

    /**
     * @return array
     */
    public function getSubjectList(): array
    {
        return $this->subjectList;
    }

    /**
     * @param array $subjectList
     */
    public function setSubjectList(array $subjectList): void
    {
        $this->subjectList = $subjectList;
    }

    /**
     * @return Moyenne
     */
    public function getMoyenne(): Moyenne
    {
        return $this->moyenne;
    }

    /**
     * @param Moyenne $moyenne
     */
    public function setMoyenne(Moyenne $moyenne): void
    {
        $this->moyenne = $moyenne;
    }

}
