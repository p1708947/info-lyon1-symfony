<?php


namespace Teckmeb\SuiviBundle\Model;


class SuiviDTO
{
    private $groupeList;

    public function __construct($groupeList)
    {
        $this->groupeList = array();
        foreach ($groupeList as $groupe) {
            $this->groupeList[] = new GroupeDTO($groupe);
        }
    }

    /**
     * @return array
     */
    public function getGroupeList(): array
    {
        return $this->groupeList;
    }

    /**
     * @param array $groupeList
     */
    public function setGroupeList(array $groupeList): void
    {
        $this->groupeList = $groupeList;
    }


}
