<?php


namespace Teckmeb\SuiviBundle\Model;


class AbsenceSuivi
{
    private $nbJustifie;
    private $nbNonJustifie;

    public function __construct($nbJustifie , $nbNonJustifie)
    {
        $this->nbJustifie = $nbJustifie;
        $this->nbNonJustifie = $nbNonJustifie;
    }

    /**
     * @return mixed
     */
    public function getNbJustifie()
    {
        return $this->nbJustifie;
    }

    /**
     * @param mixed $nbJustifie
     */
    public function setNbJustifie($nbJustifie): void
    {
        $this->nbJustifie = $nbJustifie;
    }

    /**
     * @return mixed
     */
    public function getNbNonJustifie()
    {
        return $this->nbNonJustifie;
    }

    /**
     * @param mixed $nbNonJustifie
     */
    public function setNbNonJustifie($nbNonJustifie): void
    {
        $this->nbNonJustifie = $nbNonJustifie;
    }


}
