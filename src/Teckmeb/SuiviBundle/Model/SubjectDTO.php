<?php


namespace Teckmeb\SuiviBundle\Model;


use Teckmeb\CoreBundle\Entity\Subject;
use Teckmeb\MarkBundle\Entity\Mark;

class SubjectDTO
{
    private $subject;
    private $listMark;
    private $moyenne;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->moyenne = new Moyenne();
        $this->listMark = array();
    }

    /**
     * @return Subject
     */
    public function getSubject(): Subject
    {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject(Subject $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getListMark(): array
    {
        return $this->listMark;
    }

    /**
     * @param array $listMark
     */
    public function setListMark(array $listMark): void
    {
        $this->listMark = $listMark;
    }

    /**
     * @return mixed
     */
    public function getMoyenne()
    {
        return $this->moyenne;
    }

    /**
     * @param mixed $moyenne
     */
    public function setMoyenne($moyenne): void
    {
        $this->moyenne = $moyenne;
    }

    private function addNote(Mark $mark)
    {
        $this->listMark[] = $mark;
        $this->moyenne->updateMoyenneWithMark($mark);
    }

    public function addList($markList) {
        foreach($markList as $mark) {
            $this->addNote($mark);
        }
        return $this->moyenne->calculMoyenne();
    }




}
