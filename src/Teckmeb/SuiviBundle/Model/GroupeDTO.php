<?php


namespace Teckmeb\SuiviBundle\Model;

use Teckmeb\CoreBundle\Entity\Groupe;

class GroupeDTO
{
    private $absence;
    private $groupe;
    private $ueList;
    private $moyenne;

    public function __construct(Groupe $groupe)
    {
        $this->groupe = $groupe;
        $this->moyenne = new Moyenne();
        $ueList = $groupe->getSemestre()->getCourse()->getTeachingUnits();
        $this->ueList = array();
        foreach($ueList as $ue) {
            $this->ueList[] = new UEDTO($ue);
        }
    }

    /**
     * @return mixed
     */
    public function getAbsence()
    {
        return $this->absence;
    }

    /**
     * @param mixed $absence
     */
    public function setAbsence($absence): void
    {
        $this->absence = $absence;
    }

    /**
     * @return Groupe
     */
    public function getGroupe(): Groupe
    {
        return $this->groupe;
    }

    /**
     * @param Groupe $groupe
     */
    public function setGroupe(Groupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return array
     */
    public function getUeList(): array
    {
        return $this->ueList;
    }

    /**
     * @param array $ueList
     */
    public function setUeList(array $ueList): void
    {
        $this->ueList = $ueList;
    }

    /**
     * @return Moyenne
     */
    public function getMoyenne(): Moyenne
    {
        return $this->moyenne;
    }

    /**
     * @param Moyenne $moyenne
     */
    public function setMoyenne(Moyenne $moyenne): void
    {
        $this->moyenne = $moyenne;
    }



}
