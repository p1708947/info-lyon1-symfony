<?php


namespace Teckmeb\SuiviBundle\Model;


class Moyenne
{
    private $moyenneGroupe;
    private $moyenneEleve;
    private $sommeEleve;
    private $sommeGroupe;
    private $sommeDiviseur;

    public function __construct()
    {
    }

    public function updateMoyenne($moyenneEleve, $moyenneGroupe, $coefficient)
    {
        $this->sommeEleve = ($this->sommeEleve != null) ? $this->sommeEleve + ($moyenneEleve * $coefficient) : ($moyenneEleve * $coefficient);
        $this->sommeGroupe = ($this->sommeGroupe != null) ? $this->sommeGroupe + ($moyenneGroupe * $coefficient) : ($moyenneGroupe * $coefficient);
        if ($this->sommeDiviseur != null) {
            if ($moyenneEleve != null) {
                $this->sommeDiviseur += (20 * $coefficient);
            }
        } else {
            if ($moyenneEleve != null) {
                $this->sommeDiviseur = (20 * $coefficient);
            }
        }
    }

    public function calculMoyenne() {
        if($this->sommeDiviseur != null) {
            $this->moyenneGroupe = $this->sommeGroupe / $this->sommeDiviseur * 20;
            $this->moyenneEleve = $this->sommeEleve / $this->sommeDiviseur * 20;
        }
        return array($this->moyenneEleve , $this->moyenneGroupe);
    }

    public function updateMoyenneWithMark($mark) {
        $this->sommeEleve = ($this->sommeEleve != null) ? $this->sommeEleve + ($mark->getValue() * $mark->getControl()->getCoefficient()) : ($mark->getValue() * $mark->getControl()->getCoefficient());
        $this->sommeGroupe = ($this->sommeGroupe != null) ? $this->sommeGroupe + ($mark->getControl()->getMedian() * $mark->getControl()->getCoefficient()) : ($mark->getControl()->getMedian() * $mark->getControl()->getCoefficient());
        $this->sommeDiviseur = ($this->sommeDiviseur != null) ? $this->sommeDiviseur + ($mark->getControl()->getDivisor() * $mark->getControl()->getCoefficient()) : ($mark->getControl()->getDivisor() * $mark->getControl()->getCoefficient());

    }

    /**
     * @return mixed
     */
    public function getMoyenneGroupe()
    {
        return $this->moyenneGroupe;
    }

    /**
     * @param mixed $moyenneGroupe
     */
    public function setMoyenneGroupe($moyenneGroupe): void
    {
        $this->moyenneGroupe = $moyenneGroupe;
    }

    /**
     * @return mixed
     */
    public function getMoyenneEleve()
    {
        return $this->moyenneEleve;
    }

    /**
     * @param mixed $moyenneEleve
     */
    public function setMoyenneEleve($moyenneEleve): void
    {
        $this->moyenneEleve = $moyenneEleve;
    }


}
