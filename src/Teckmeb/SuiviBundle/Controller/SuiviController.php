<?php

namespace Teckmeb\SuiviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Teckmeb\SuiviBundle\Model\AbsenceSuivi;
use Teckmeb\SuiviBundle\Model\SuiviDTO;

class SuiviController extends Controller
{
    public function homeAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat et aux professeurs.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Student');
        $listStudent = $repository->findAll();
        return $this->render('TeckmebSuiviBundle::home.html.twig', array(
            'listStudent' => $listStudent,
            'Suivi' => true,
        ));
    }


    public function profileAction($id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité au secretariat et aux professeurs.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Student');
        $student = $repository->find($id);
        if (null === $student) {
            throw new NotFoundHttpException("Cet étudiant d'id " . $id . " n'existe pas.");
        }
        $suiviDTO = new SuiviDTO($student->getGroupes());

        foreach ($suiviDTO->getGroupeList() as $groupe) {

            list($nbJus, $nbNotJus) = $this->getAbsences($groupe->getGroupe(), $student);
            $groupe->setAbsence(new AbsenceSuivi($nbJus, $nbNotJus));

            $repository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('TeckmebMarkBundle:Mark');
            foreach ($groupe->getUeList() as $ue) {
                foreach ($ue->getSubjectList() as $subject) {
                    list($moyenneEleve , $moyenneGroupe) = $subject->addList($repository->myFindAllMark($student, $groupe->getGroupe(), $subject->getSubject()));
                    $ue->getMoyenne()->updateMoyenne($moyenneEleve , $moyenneGroupe, $subject->getSubject()->getSubjectCoefficient());
                    $groupe->getMoyenne()->updateMoyenne($moyenneEleve , $moyenneGroupe, $subject->getSubject()->getSubjectCoefficient());
                }
                $ue->getMoyenne()->calculMoyenne();
            }
            $groupe->getMoyenne()->calculMoyenne();
        }

        return $this->render('TeckmebSuiviBundle::view.html.twig', array(
            'suiviDTO' => $suiviDTO,
            'Student' => $student,
            'Suivi' => true,
        ));
    }

    public function getAbsences($groupe, $student)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebAbsenceBundle:Absence');
        $listAbsence = $repository->myCountAbsenceByGroupeStudent($groupe, $student);
        $numJus = ($listAbsence[0][1] != null) ? $listAbsence[0][1] : 0;
        $numNonJus = $listAbsence[0][2] - $numJus;
        return array($numJus, $numNonJus);
    }
}
