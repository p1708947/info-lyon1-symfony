<?php


namespace Teckmeb\CoreBundle\Services;


class DateService
{
    const JOUR_SEMAINE_ARRAY = array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche");
    const JOUR_SEMAINE_RAC_ARRAY = array("Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim");
    const MOIS_ARRAY = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    const MOIS_RAC_ARRAY = array("Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc");

    public static function getDateNow()
    {
        $now = new \DateTime();
        $now->setTimezone(new \DateTimeZone("Europe/Paris"));
        return $now;
    }

    public static function getDecallageHoraire()
    {
        $decallageHoraireDate = new \DateTime();
        $decallageHoraireDate->setTimezone(new \DateTimeZone("Europe/Paris"));
        return $decallageHoraireDate->format("P");
    }

    public function translateDate(\Datetime $date, $withRac = true)
    {
        list($jourSemaine, $jour, $mois) = explode(" ", $date->format("w d m"));
        if ($withRac) {
            $jourSemaine = $this->translateJourSemaineRac($jourSemaine);
            $mois = $this->translateMoisRac($mois);
        } else {
            $jourSemaine = $this->translateJourSemaine($jourSemaine);
            $mois = $this->translateMois($mois);
        }
        return $jourSemaine . " " . $jour . " " . $mois;
    }

    private function translateJourSemaineRac($jourSemaine)
    {
        return self::JOUR_SEMAINE_RAC_ARRAY[($jourSemaine - 1) % count(self::JOUR_SEMAINE_RAC_ARRAY)];
    }

    private function translateMoisRac($mois)
    {
        return self::MOIS_RAC_ARRAY[($mois - 1) % count(self::MOIS_RAC_ARRAY)];
    }

    private function translateJourSemaine($jourSemaine)
    {
        return self::JOUR_SEMAINE_ARRAY[($jourSemaine - 1) % count(self::JOUR_SEMAINE_ARRAY)];
    }

    private function translateMois($mois)
    {
        return self::MOIS_ARRAY[($mois - 1) % count(self::MOIS_ARRAY)];
    }

    private function translateJour()
    {
    }
}