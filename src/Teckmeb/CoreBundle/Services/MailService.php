<?php

namespace Teckmeb\CoreBundle\Services;

use Teckmeb\UserBundle\Entity\User;

class MailService
{
    const ISACTIF = false;

    protected $swift_mail;
    protected $templating;
    protected $mail_sender;
    protected $passwordService;
    protected $userManager;

    public function __construct($swift_mail, $templating, $mail_sender, $passwordService, $userManager)
    {
        $this->swift_mail = $swift_mail;
        $this->templating = $templating;
        $this->mail_sender = $mail_sender;
        $this->passwordService = $passwordService;
        $this->userManager = $userManager;
    }

    public function addUserMail(User $user, $isTeacher, $password = null)
    {
        if ($password == null) {
            $password = $this->passwordService->getRandomPassword();
            $user->setPlainPassword($password);
            $this->userManager->updateUser($user);
        }

        if ($isTeacher) {
            $this->sendMail("Création de votre compte sur Teckmeb", $this->mail_sender, $user->getEmailCanonical(), "TeckmebCoreBundle:Mails:addTeacher.html.twig", array("content" => $user, "password" => $password));
        } else {
            $this->sendMail("Création de votre compte sur Teckmeb", $this->mail_sender, $user->getEmailCanonical(), "TeckmebCoreBundle:Mails:addStudent.html.twig", array("content" => $user, "password" => $password));
        }
    }

    public function sendMail($subject, $from, $to, $contentLocation, $content)
    {
        if (!self::ISACTIF) {
            return;
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->templating->render(
                    $contentLocation,
                    $content
                ),
                'text/html'
            );
        $this->swift_mail->send($message);
    }

    public function updatePasswordMail(User $user, $password = null)
    {
        if ($password == null) {
            $password = $this->passwordService->getRandomPassword();
            $user->setPlainPassword($password);
            $this->userManager->updateUser($user);
        }
        $this->sendMail("Modification de votre compte sur Teckmeb", $this->mail_sender, $user->getEmailCanonical(), "TeckmebAdministrationBundle:Mails:updatePassword.html.twig", array("content" => $user, "password" => $password));
    }
}
