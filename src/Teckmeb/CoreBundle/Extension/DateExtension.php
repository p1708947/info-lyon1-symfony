<?php


namespace Teckmeb\CoreBundle\Extension;


use Teckmeb\CoreBundle\Services\DateService;

class DateExtension extends \Twig_Extension
{

    protected $dateService;

    public function __construct(DateService $dateService)
    {
        $this->dateService = $dateService;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('translateDate', array($this->dateService, 'translateDate')),
        );
    }

    public function getName()
    {
        return 'InfopartDate';
    }
}