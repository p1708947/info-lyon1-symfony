<?php

namespace Teckmeb\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class GroupeEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('students', EntityType::class, array(
                'class' => 'TeckmebCoreBundle:Student',
                'multiple' => true,
                'expanded' => true));
    }

    public function getParent()
    {
        return GroupeType::class;
    }
}