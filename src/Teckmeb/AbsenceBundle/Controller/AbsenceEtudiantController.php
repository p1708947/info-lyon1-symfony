<?php

namespace Teckmeb\AbsenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Teckmeb\AbsenceBundle\Model\ListGroupeDTO;

class AbsenceEtudiantController extends Controller
{

    public function viewAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT')) {
            // Sinon on déclenche une exception « Accès interdit »
            throw new AccessDeniedException('Accès limité aux élèves.');
        }
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebCoreBundle:Student');
        $student = $repository->findOneByUser($this->getUser()->getId());

        $listGroupeDTO = new ListGroupeDTO($student->getGroupes());

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('TeckmebAbsenceBundle:Absence');
        foreach($listGroupeDTO->getListGroupe() as $groupe) {
            $groupe->addList($repository->myFindAbsenceByGroupeStudent($groupe->getGroupe(), $student));
        }

        return $this->render('TeckmebAbsenceBundle::view.html.twig', array(
            'listGroupeDTO' => $listGroupeDTO,
        ));
    }
}

