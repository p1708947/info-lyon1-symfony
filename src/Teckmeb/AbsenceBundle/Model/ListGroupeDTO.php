<?php


namespace Teckmeb\AbsenceBundle\Model;


class ListGroupeDTO
{
    private $listGroupe;

    public function __construct($listGroupe)
    {
        $this->listGroupe = array();
        foreach($listGroupe as $groupe) {
            $this->listGroupe[] = new GroupeDTO($groupe);
        }
    }

    /**
     * @return array
     */
    public function getListGroupe(): array
    {
        return $this->listGroupe;
    }

    /**
     * @param array $listGroupe
     */
    public function setListGroupe(array $listGroupe): void
    {
        $this->listGroupe = $listGroupe;
    }
}
