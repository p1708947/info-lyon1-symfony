<?php


namespace Teckmeb\AbsenceBundle\Model;


use Teckmeb\AbsenceBundle\Entity\Absence;
use Teckmeb\CoreBundle\Entity\Groupe;

class GroupeDTO
{
    private $groupe;
    private $listAbsence;

    public function __construct(Groupe $groupe)
    {
        $this->groupe = $groupe;
        $this->listAbsence = array();
    }

    public function add(Absence $absence)
    {
        $this->listAbsence[] = $absence;
    }

    public function addList($absenceList)
    {
        foreach ($absenceList as $absence) {
            $this->add($absence);
        }
    }

    /**
     * @return Groupe
     */
    public function getGroupe(): Groupe
    {
        return $this->groupe;
    }

    /**
     * @param Groupe $groupe
     */
    public function setGroupe(Groupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    /**
     * @return array
     */
    public function getListAbsence(): array
    {
        return $this->listAbsence;
    }

    /**
     * @param array $listAbsence
     */
    public function setListAbsence(array $listAbsence): void
    {
        $this->listAbsence = $listAbsence;
    }
}
