<?php

namespace Teckmeb\ControlBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class ControlEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('controlType')
            ->remove('education');
    }

    public function getParent()
    {
        return ControlType::class;
    }
}
