<?php

namespace Teckmeb\ControlBundle\Model;

use Teckmeb\ControlBundle\Entity\ControlType;
use Teckmeb\CoreBundle\Entity\Groupe;
use Teckmeb\CoreBundle\Entity\Subject;

class FiltreControl
{

    public $subject = null;
    public $groupe = null;
    public $typeControl = null;

    public function getAll()
    {
        return array(($this->subject != null) ? $this->subject : null, ($this->groupe != null) ? $this->groupe : null, ($this->typeControl != null) ? $this->typeControl : null);
    }

    public function getTypeControl()
    {
        return $this->typeControl;
    }

    public function setTypeControl(ControlType $typeControl)
    {
        $this->typeControl = $typeControl;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject(Subject $subject)
    {
        $this->subject = $subject;
    }

    public function getGroupe()
    {
        return $this->groupe;
    }

    public function setGroupe(Groupe $groupe)
    {
        $this->groupe = $groupe;
    }
}