<?php

namespace Teckmeb\TimeTableBundle\Service;

class ColorService
{
    public $colorTab;
    public $tabColor;
    protected $session;

    public function __construct($requestObj)
    {
        $this->session = $requestObj->getCurrentRequest()->getSession();
        $this->tabColor = $this->session->get('tabColor');
        $tmp = $this->session->get('colorTab');
        if (($tmp != null) && (count($tmp) != 0)) {
            $this->setColorTab($tmp);
        } else {
            $this->setColorTab(array(
                '#ff5252', '#ff1744', '#ff4081', '#c51162',
                '#e040fb', '#aa00ff', '#7c4dff', '#6200ea',
                '#536dfe', '#82b1ff', '#00b0ff', '#00e5ff',
                '#64ffda', '#00bfa5', '#00e676', '#76ff03',
                '#eeff41', '#ffff00', '#ffab00', '#ff6e40',
                '#9e9e9e', '#607d8b'
            ));
        }
        if ($this->tabColor === null)
            $this->tabColor = array();
    }

    public function save()
    {
        $this->session->set("tabColor", $this->tabColor);
        $this->session->set("colorTab", $this->colorTab);
    }

    public function getColorTab()
    {
        return $this->colorTab;
    }

    public function setColorTab($colorTab)
    {
        $this->colorTab = $colorTab;
    }

    public function getColorFromNumModule($numModule)
    {
        if (!(isset($this->tabColor[$numModule])))
            $this->tabColor[$numModule] = $this->getRandomColor();
        return $this->tabColor[$numModule];
    }

    public function getRandomColor()
    {
        $chiffreTab = "0123456789abcdef";
        if (count($this->colorTab) == 0) {
            for ($i = 0; $i < 9; $i++) {
                $color = "#";
                for ($index = 0; $index < 6; $index++) {
                    $color = $color . $chiffreTab[rand(0, 15)];
                }
                $this->colorTab[] = $color;
            }
            $this->colorTab = array_values($this->colorTab);
        }
        $min = 0;
        $max = count($this->colorTab) - 1;
        $nbColor = rand($min, $max);
        $Color = $this->colorTab[$nbColor];
        unset($this->colorTab[$nbColor]);
        $this->setColorTab(array_values($this->colorTab));
        return $Color;
    }

    public function resetAll() {
        $this->setColorTab(null);
        $this->session->set("colorTab" , null);
        $this->session->set("tabColor" , null);
    }
}
