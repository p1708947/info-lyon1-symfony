<?php

namespace Teckmeb\TimeTableBundle\Service;

use Doctrine\ORM\EntityManager;
use Teckmeb\TimeTableBundle\Entity\Timetable;
use Teckmeb\TimeTableBundle\Exception\TimetableException;
use Teckmeb\TimeTableBundle\Model\CoursDTO;
use Teckmeb\TimeTableBundle\Model\TimetableDTO;
use Teckmeb\UserBundle\Entity\User;

class TimetableService
{

    protected $doctrine;
    protected $colorService;

    public function __construct(EntityManager $doctrine, $colorService)
    {
        $this->doctrine = $doctrine;
        $this->colorService = $colorService;
    }

    public function getTimetableFromUser(User $user)
    {
        return $this->getRepository()->findOneByUser($user);
    }

    private function getRepository()
    {
        return $this->doctrine->getRepository("TeckmebTimeTableBundle:Timetable");
    }

    public function initTimetable()
    {
        return new Timetable();
    }

    public function updateRessourceFromUrl(Timetable $timetable, $flush = true)
    {
        $url = $timetable->getResource();
        // On récupère les differents paramêtres de l'url notamment resources qui nous intéresse
        $arr = parse_url($url);
        $parameters = $arr["query"];
        parse_str($parameters, $data);
        // Si l'attribut resources n'existe pas on créer une erreur sinon on l'enregistre
        if (isset($data['resources']) && isset($data['projectId'])) {
            $resource = $data['resources'];
            $projectId = $data['projectId'];
        } else {
            throw new TimetableException("Erreur dans l'url");
        }
        $timetable->setResource($resource);
        $timetable->setProjectId($projectId);
        // On ajout l'objet timetable à la base de donnée
        $this->doctrine->persist($timetable);
        if ($flush)
            $this->doctrine->flush();
    }

    public function getOtherDate($dateDebut)
    {
        // Création des variables pour permettre de changer de semaine
        $datePrec = new \DateTime($dateDebut->format('Y-m-d') . '-7day');
        $dateSuiv = new \DateTime($dateDebut->format('Y-m-d') . '+7day');
        return array($datePrec, $dateSuiv);
    }

    public function getTimetableFromDate($timetable, $periodeEDT, $isDashboard = false)
    {
        $dateDebut = $periodeEDT->getDateDebut();
        $dateFin = $periodeEDT->getDateFin();
        // On récupère le fichier ics contenant les cours grâce à la resource et a une date de début et de fin
        $url = $this->getUrlFromTimetable($timetable, $dateDebut, $dateFin);
        $calendar = file_get_contents($url);
        // On défini tous les REGEX utilisé pour parcourir le fichier et récupérer les infos voulu
        $regExpCours = '/SUMMARY:(.*)/';
        $regExpDate = '/DTSTART:(.*)/';
        $regExpDesc = '/DESCRIPTION:(.*)/';
        $regExpDateF = '/DTEND:.(.*)/';
        $regExpExport = '/\(Exporté/';
        $regExpLocation = '/LOCATION:(.*)/';
        // $n permet de savoir le nombre de cours est présent et grâce a preg_match_all on récupèrer chaque contenu des cours
        $n = preg_match_all($regExpCours, $calendar, $coursTab, PREG_PATTERN_ORDER);
        preg_match_all($regExpDate, $calendar, $dateTab, PREG_PATTERN_ORDER);
        preg_match_all($regExpDesc, $calendar, $descTab, PREG_PATTERN_ORDER);
        preg_match_all($regExpDateF, $calendar, $dateFtab, PREG_PATTERN_ORDER);
        preg_match_all($regExpLocation, $calendar, $locationTab, PREG_PATTERN_ORDER);
        // Initialisation du tableau contenant chaque jour de la semaine pour trier les données reçus

        $timetableDTO = new TimetableDTO($dateDebut, ($isDashboard) ? 1 : 5, ($n > 0));

        // On parcours tout les évènements et on créer les variables nécessaires
        for ($j = 0; $j < $n; $j++) {

            $coursDTO = new CoursDTO();
            $coursDTO->setLocation($locationTab[0][$j]);
            $coursDTO->setDateDebut($this->updateDate($dateTab[0][$j]));
            $coursDTO->setDateFin($this->updateDate($dateFtab[0][$j]));
            $coursDTO->updateDuree();
            $coursDTO->setDescriptionAndGroupes($descTab[0][$j]);
            $coursDTO->setNumModule($coursTab[0][$j]);
            $coursDTO->setCours($coursTab[0][$j]);
            $coursDTO->setColor($this->colorService->getColorFromNumModule($coursDTO->getNumModule()));

            $timetableDTO->add($coursDTO);
        }

        $this->colorService->save();
        return $timetableDTO;
    }

    public function getUrlFromTimetable(Timetable $timetable, $dateDebut, $dateFin)
    {
        return "http://adelb.univ-lyon1.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=" . $timetable->getResource() . "&projectId=" . $timetable->getProjectId() . "&calType=ical&firstDate=" . $dateDebut->format('Y-m-d') . "&lastDate=" . $dateFin->format('Y-m-d');
    }

    public function updateDate($date)
    {
        $date = new \DateTime(explode(":", $date)[1]);
        $date->setTimezone(new \DateTimeZone("Europe/Paris"));
        return $date;
    }

    public function horaireSuiv($horaire)
    {
        if (substr($horaire, -2) == "00") {
            return substr($horaire, 0, 3) . "30";
        } else {
            $return = substr($horaire, 0, 2) + 1;
            $return = ($return > 9) ? $return . ":00" : "0" . $return . ":00";
            return $return;
        }
    }

    function dureeToNumeric($duree)
    {
        $hour = substr($duree, 0, -3);
        $min = substr($duree, -2);
        return ($hour * 2) + ((int)($min / 30));
    }

    private function returnDayNom($num)
    {
        $tab = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
        return $tab[$num];
    }
}
