<?php

namespace Teckmeb\TimeTableBundle\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

class TimetableException extends Exception
{

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }
}
