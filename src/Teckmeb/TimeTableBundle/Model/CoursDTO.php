<?php


namespace Teckmeb\TimeTableBundle\Model;


class CoursDTO
{
    private $location;
    private $dateDebut;
    private $dateFin;
    private $duree;
    private $groupes;
    private $color;
    private $numModule;
    private $description;
    private $cours;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $location = substr($location, 9);
        $location = str_replace('\\', '', $location);
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * @param mixed $duree
     */
    public function setDuree($duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return mixed
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * @param mixed $groupes
     */
    public function setGroupes($groupes): void
    {
        $this->groupes = $groupes;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getNumModule()
    {
        return $this->numModule;
    }

    /**
     * @param mixed $numModule
     */
    public function setNumModule($numModule): void
    {
        $cours = substr($numModule, 8);
        $cours = str_replace('\,', ',', $cours);
        $this->numModule = explode(" ", $cours)[0];
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCours()
    {
        return $this->cours;
    }

    /**
     * @param mixed $cours
     */
    public function setCours($cours): void
    {
        $cours = substr($cours, 8);
        $cours = str_replace('\,', ',', $cours);
        $this->cours = $cours;
    }

    public function updateDuree()
    {
        $this->duree = ($this->getDateFin()->getTimestamp() - $this->getDateDebut()->getTimestamp()) / 1800;
    }

    /**
     * @return mixed
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param mixed $dateFin
     */
    public function setDateFin($dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @return mixed
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param mixed $dateDebut
     */
    public function setDateDebut($dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    public function setDescriptionAndGroupes($descTab)
    {
        $desc = substr($descTab, 12);
        // On supprimer la fin du contenu de la variable description pour seulement récupérer la description du cours
        $tmp = mb_strripos($desc, '(');
        $desc = substr($desc, 0, $tmp);

        // On récupère le groupe
        $regex = '#G[1-9]S[1-4]#';
        $tabGroupe = array();
        $tabRetourGroupe = array();
        $p = preg_match_all($regex, $desc, $tabRetourGroupe, PREG_OFFSET_CAPTURE);
        for ($i = 0; $i < $p; $i++) {
            $tabGroupe[] = $tabRetourGroupe[0][$i][0];
        }
        if ($p > 0) {
            $desc = substr($desc, $tabRetourGroupe[0][$p - 1][1] + 4);
            $groupe = implode(' , ', $tabGroupe);
        } else {
            $groupe = explode('\n', $desc);
            if (isset($groupe[1])) {
                $groupe = $groupe[1];
                $desc = str_replace($groupe, '', $desc);
            } else
                $groupe = null;
        }
        // On supprimer les caractères spécieux de la description
        $desc = str_replace('\n', ' ', $desc);

        $this->setDescription($desc);
        $this->setGroupes($groupe);
    }

}