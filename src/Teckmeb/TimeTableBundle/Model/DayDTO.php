<?php


namespace Teckmeb\TimeTableBundle\Model;


class DayDTO
{
    const HoraireList = ["08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30"];
    private $day;
    private $listHoraire = array();
    private $asContent = false;

    public function __construct(\DateTime $date)
    {
        $this->day = $date;
        foreach (self::HoraireList as $horaireString) {
            $this->listHoraire[] = new HoraireDTO($horaireString);
        }
    }

    /**
     * @return \DateTime
     */
    public function getDay(): \DateTime
    {
        return $this->day;
    }

    /**
     * @param \DateTime $day
     */
    public function setDay(\DateTime $day): void
    {
        $this->day = $day;
    }

    /**
     * @return array
     */
    public function getListHoraire(): array
    {
        return $this->listHoraire;
    }

    /**
     * @param array $listHoraire
     */
    public function setListHoraire(array $listHoraire): void
    {
        $this->listHoraire = $listHoraire;
    }

    /**
     * @return bool
     */
    public function isAsContent(): bool
    {
        return $this->asContent;
    }

    /**
     * @param bool $asContent
     */
    public function setAsContent(bool $asContent): void
    {
        $this->asContent = $asContent;
    }


    public function add(CoursDTO $coursDTO)
    {
        if (!$this->asContent) {
            $this->asContent = true;
        }
        $currentIndice = $this->getIndexForHoraire($coursDTO->getDateDebut()->format("H:i"));
        $horaireDTO = $this->listHoraire[$currentIndice];
        $horaireDTO->setCoursDTO($coursDTO);
        $horaireDTO->setIsActif(true);
        $horaireDTO->setIsPrimary(true);
        for ($i = ++$currentIndice; $i < ($currentIndice + $coursDTO->getDuree() - 1); $i++) {
            $currentHoraireDTO = $this->listHoraire[$i];
            $currentHoraireDTO->setIsActif(true);
            $currentHoraireDTO->setIsPrimary(false);
        }
    }

    public function getIndexForHoraire($horaireString)
    {
        foreach (self::HoraireList as $i => $horaire) {
            if ($horaireString === $horaire)
                return $i;
        }
    }

    public function getNextHoraire($horaire)
    {
        if (substr($horaire, -2) == "00") {
            return substr($horaire, 0, 3) . "30";
        } else {
            $return = substr($horaire, 0, 2) + 1;
            $return = ($return > 9) ? $return . ":00" : "0" . $return . ":00";
            return $return;
        }
    }

    private function getHoraireFromDateTime(\DateTime $dateTime)
    {
        return $this->listHoraire[$this->getIndexForHoraire($dateTime->format("H:i"))];
    }


}