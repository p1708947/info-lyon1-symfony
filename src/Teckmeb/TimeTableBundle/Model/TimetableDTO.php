<?php


namespace Teckmeb\TimeTableBundle\Model;


class TimetableDTO
{
    private $dayDTOList = array();
    private $asContent;
    private $nbJour;

    public function __construct(\DateTime $dateDebut, $nbJour, $asContent)
    {
        $this->asContent = $asContent;
        for ($i = 0; $i < $nbJour; $i++) {
            $this->dayDTOList[] = new DayDTO(new \DateTime($dateDebut->format('Y-m-d') . '+' . $i . 'day'));
        }

        $this->nbJour = $nbJour;
    }

    public function add(CoursDTO $coursDTO)
    {
        $this->getGoodDayForAdd($coursDTO)->add($coursDTO);
    }

    public function getGoodDayForAdd(CoursDTO $coursDTO)
    {
        if (count($this->dayDTOList) > 1) {
            foreach ($this->dayDTOList as $dayDTO) {
                if ($this->compareDate($dayDTO->getDay(), $coursDTO->getDateDebut())) {
                    return $dayDTO;
                }
            }
        } else {
            return $this->dayDTOList[0];
        }
    }

    private function compareDate(\DateTime $a, \DateTime $b)
    {
        return ($a->format("d") === $b->format("d"));
    }

    /**
     * @return array
     */
    public function getDayDTOList(): array
    {
        return $this->dayDTOList;
    }

    /**
     * @param array $dayDTOList
     */
    public function setDayDTOList(array $dayDTOList): void
    {
        $this->dayDTOList = $dayDTOList;
    }

    /**
     * @return mixed
     */
    public function getAsContent()
    {
        return $this->asContent;
    }

    /**
     * @param mixed $asContent
     */
    public function setAsContent($asContent): void
    {
        $this->asContent = $asContent;
    }

    /**
     * @return mixed
     */
    public function getNbJour()
    {
        return $this->nbJour;
    }

    /**
     * @param mixed $nbJour
     */
    public function setNbJour($nbJour): void
    {
        $this->nbJour = $nbJour;
    }



}
