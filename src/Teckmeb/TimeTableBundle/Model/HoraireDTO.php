<?php


namespace Teckmeb\TimeTableBundle\Model;


class HoraireDTO
{
    private $isActif = false;
    private $isPrimary = false;
    private $coursDTO;
    private $horaireString;

    public function __construct($horaireString)
    {
        $this->horaireString = $horaireString;
    }

    /**
     * @return bool
     */
    public function isActif(): bool
    {
        return $this->isActif;
    }

    /**
     * @param bool $isActif
     */
    public function setIsActif(bool $isActif): void
    {
        $this->isActif = $isActif;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }

    /**
     * @param bool $isPrimary
     */
    public function setIsPrimary(bool $isPrimary): void
    {
        $this->isPrimary = $isPrimary;
    }

    /**
     * @return mixed
     */
    public function getCoursDTO()
    {
        return $this->coursDTO;
    }

    /**
     * @param mixed $coursDTO
     */
    public function setCoursDTO($coursDTO): void
    {
        $this->coursDTO = $coursDTO;
    }

    /**
     * @return mixed
     */
    public function getHoraireString()
    {
        return $this->horaireString;
    }

    /**
     * @param mixed $horaireString
     */
    public function setHoraireString($horaireString): void
    {
        $this->horaireString = $horaireString;
    }


}